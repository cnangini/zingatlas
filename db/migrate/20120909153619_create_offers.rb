class CreateOffers < ActiveRecord::Migration
  def self.up
    create_table :offers do |t|
      t.integer :itemrequest_id
      t.integer :user_id
      t.date :cando_date
      t.timestamps
    end
  end

  def self.down
    drop_table :offers
  end
end
