class CreateItemrequests < ActiveRecord::Migration
  def self.up
    create_table :itemrequests do |t|
      t.integer :user_id
      t.string :uid
      t.string :getorsend
      t.string :name
      t.string :destination
      t.string :origin
      t.text :description
      t.float :weight
      t.date :completiondate
      t.timestamps
    end
  end

  def self.down
    drop_table :itemrequests
  end
end
