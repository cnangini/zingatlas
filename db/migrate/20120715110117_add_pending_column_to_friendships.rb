class AddPendingColumnToFriendships < ActiveRecord::Migration
  def change
    add_column :friendships, :pending, :integer
  end
end
