class CreateTravelinfos < ActiveRecord::Migration
  def self.up
    create_table :travelinfos do |t|
      t.string :user_id
      t.string :uid
      t.string :origin
      t.string :destination
      t.date :departdate
      t.date :returndate
      t.string :willship
      t.timestamps
    end
  end

  def self.down
    drop_table :travelinfos
  end
end
