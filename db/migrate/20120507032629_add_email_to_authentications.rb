class AddEmailToAuthentications < ActiveRecord::Migration
  def change
    add_column :authentications, :email, :string
    add_column :authentications, :name, :string
    add_column :authentications, :token, :string
  end
end
