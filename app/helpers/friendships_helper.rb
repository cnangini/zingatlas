module FriendshipsHelper

  #CN: moved to user.rb and initialized in pages_controller.rb
  #def pending_forward
  #  #this function returns email of current user's friend if
  #  #current user is in friendships table with selected friend (forward relationship)
  #		
  #  @ufr = current_user.friendships 
  #	#finfo = Hash.new
  #	
  #	#fid = Integer(params[:friend_id])
  #	
  #	#check forward relationship
  #	flist = []	  
  #  @ufr.each do |f|  
  #    if f.pending == 1	
  #    #return f.friend_id
  #	    ux = User.find_by_id(f.friend_id) 	
  #	    flist << [ux, f] #ux.email	
  #	  end
  #  end
  #	flist

  #end
  
  
#$$ To test pending_forward with 2 outputs
  def miika
    #this function returns email of current user's friend if
    #current user is in friendships table with selected friend (forward relationship)
		
    @ufr = current_user.friendships 
	#finfo = Hash.new
	
	#fid = Integer(params[:friend_id])
	
	#check forward relationship
	flist = []	  
    @ufr.each do |f|  
      if f.pending == 1	
      #return f.friend_id
	    ux = User.find_by_id(f.friend_id) 	
	    #flist << [ux, f] #ux.email	
	    flist << f
	  end
    end
	flist

  end
#$$  
  
  
#  def pending_inverse
#    @user = User.all()
    #this function returns emails of users who have sent a friend request to current_user
	#calling inverse_friendships instead of inverse_friends
#	@requester = current_user.inverse_friendships.find(:all)
	
	
	#@requester = current_user.inverse_friends.find(:all)
	
	# for i in 0..@cuf.size
	#   if @current_user.friendships[i].pending != 2
	#     @requester = current_user.inverse_friends.find(:all)
	#   end
	# end
	 
    #THIS CHECK FOR PENDING==1 DOES NOT WORK	 
	#check pending col. If pending === 2, current_user has already accepted request 
	#flist=[]  
    #  @requester.each do |r| 
    #    #read pending col 
	#    @user.each_with_index do |f,index| 
	#      if index == r.user_id - 1 
    #        #pending = f.friendships[0].pending 
	#		if f.friendships[0].pending == 1 			   
	#		  #@requester_confirmed = current_user.inverse_friendships.find(:r)
	#		  requester_confirmed = r 
	#		  flist << r
	#		end 
	#	  end
    #    end #f loop
    #  end #r loop
  	
 # end
  
  #CN: moved to user.rb and initialized in pages_controller.rb
  #def pending_inverse
    #@requester = current_user.inverse_friendships
	
	#flist=[]
	#@requester.each do |r| 
    #  if r.pending == 1 
    #    ux = User.find_by_id(r.user_id) 
	#    flist << [ux.email, r] 
    #  end 
	#end
    #flist
  #end
  
  # Miika Pihjala: This is not used anymore, it is moved to model user.rb
  #
  #
  #def unconnected_users
    #@user = User.all()
	#@cuf = current_user.friendships
	#@requester = current_user.inverse_friendships.find(:all)
	
	##Outputs the emails of all unconnected users.
	##Logic: initialize a flag array to zero, and change entry to 1 if user has either been 
    ##sent a friend request by current_user, or has sent a friend request to current_user 

	#flist=[]
    ##initialize flag array 
    #flaguser_array=[] 
    #@user.each_with_index do |f, index| 
      #flaguser_array[index] = 0 
    #end
	
	##flag users who were sent friend requests by current_user 
    #@cuf.each do |c|
      #ux = User.find_by_id(c.friend_id)
  
      #@user.each_with_index do |f, index| 
        #if f.email == ux.email 
				#flaguser_array[index] = 1
			#end 
      #end
    #end

	##flag users who sent friend requests to current_user 
	#@requester.each do |r| 
      #ind = r.user_id - 1 #NB: array indices start at 0!!! 
      #flaguser_array[ind] = 1
    #end 

	##save only users whose corresponding flaguser_array index is 0
    #@user.each_with_index do |f, index| 
      #if flaguser_array[index] == 0 
        #if f.email != current_user.email 
			##flist << f.email	
          #flist << f		  
			#end
      #end
    #end

    #flist
  #end
    
  
  
    

end
