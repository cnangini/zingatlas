class AuthenticationsController < ApplicationController
  def index
    # all authentication methods assigned to current user
    @authentications = current_user.authentications.all if current_user
  end

  def create
    # get the hash received from the provider
    omniauth = request.env["omniauth.auth"] 
    if omniauth and provider = omniauth['provider'] and uid = omniauth['uid']
    else
      flash[:notice] = "Error authenticating through <" + provider + "> (route: " + params[:provider] + ")"
      redirect_to new_user_registration_url 
      return
    end

    if user_signed_in? 
      # check whether this provider already assigned to the user -- if not add it
      if auth = Authentication.find_by_provider_and_uid(provider, uid)
        if auth.user== current_user
          flash[:notice] = provider + " already linked to your account."
        else
          flash[:notice] = "This " + provider + " is account linked to another user -- TODO handle more gracefully.." 
        end
        redirect_to authentications_path
      else 
        current_user.apply_omniauth(omniauth)
        redirect_to authentications_path
      end
    else 
      # not signed in -- find user corresponding to the auth data if any
      user = User.find_for_omniauth(omniauth, current_user)
      if user and user.persisted?
        flash[:notice] = "Signed in successfully via " + provider 
        sign_in_and_redirect :user, user
        # or should this be user, :event=>authentication ??
      else
        session[:omniauth] = omniauth
        flash[:notice] = "Need more info.. No email provided .."
        redirect_to new_user_registration_url 
      end
    end
  end

  def destroy
    @authentication = current_user.authentications.find(params[:id])
    @authentication.destroy
    flash[:notice] = "Successfully destroyed authentication."
    redirect_to authentications_path
  end

end
