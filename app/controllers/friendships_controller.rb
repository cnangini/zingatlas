class FriendshipsController < ApplicationController

  #for search bar
  def index
    @friendships = Friendship.search(params[:search])
  end

  def create
    #check if user is already a friend then return error, else send friend request ##### add as friend
	@userfriends = current_user.friendships
	@flag_req = 0
	
	fid = Integer(params[:friend_id])
	
	@userfriends.each do |ufr|
		if (ufr.friend_id == fid)
			@flag_req = 1
			#logger.debug "found "
		#else
			#logger.debug "looking, but not found yet #{ufr.friend_id} and #{fid}"
		end
	end

	if @flag_req == 1
		flash[:notice] = "Already friend."
		redirect_to userlist_path
		logger.debug "User already exist"
	else
	  	@friendship = current_user.friendships.build(:friend_id => params[:friend_id], :pending => 1)
		if @friendship.save
		  #flash[:notice] = "Added friend."
		  #redirect_to root_url
		  redirect_to userlist_path
		  flash[:notice] = "Friend request sent."
		else
		  flash[:notice] = "Unable to add friend."
		  redirect_to root_url
		end
	  	
	end
	
  end
  
  def destroy
    #@friendship = current_user.friendships.find(params[:id])
	@friendship = current_user.friendships
	@requester = current_user.inverse_friendships
	
	#CN edits to take care of case where current_user wants to remove a friend that they have accepted 	
	if @friendship.find_by_id(params[:id])
	  @friendship.find_by_id(params[:id]).destroy
	  flash[:notice] = "Removed friendship (initiated by current_user)"
	else
	  @requester.find_by_id(params[:id]).destroy
	  flash[:notice] = "Removed friendship (initiated by other user)"
	end
	
  #  @friendship.destroy
  #  flash[:notice] = "Removed friendship."
    redirect_to userlist_path #current_user
  end
  
  def update
    #need to call inverse_friendships because need the user_id of the friend, NOT the current_user
	@friendship = current_user.inverse_friendships.find_by_id(params[:id])
	@friendship.update_attributes(:pending => 2)
	
	if @friendship.save
		#flash[:notice] = "Added friend."
		#redirect_to root_url
		redirect_to userlist_path
		flash[:notice] = "Friendship accepted."
	else
		flash[:notice] = "Unable to add friend."
		redirect_to root_url
	end
	#from Selva:
	#@friendship = current_user.friendships.find_by_user_id(params[:friend_id])
	#@friendship.updated_attributes(pending => 2)
  end
  
  
end
