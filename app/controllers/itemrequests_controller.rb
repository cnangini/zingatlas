class ItemrequestsController < ApplicationController
  def index
    @itemrequests = Itemrequest.all     #.find(params[:user_id]) 
	@my_friendlist_users = current_user.my_friendlist
  end

  def show
    @itemrequest = Itemrequest.find(params[:id])
  end
  
  def new
    @itemrequest = Itemrequest.new
  end
  
  def create
	logger.debug "ITEMREQUEST: CREATE"
    @itemrequest = Itemrequest.new(params[:itemrequest])
    if @itemrequest.save
      redirect_to @itemrequest, :notice => "Successfully created itemrequest."
    else
      render :action => 'new'
    end
  end

  def edit
    @itemrequest = Itemrequest.find(params[:id])
	#CN edits because edits are not saved!!!
	#@itemrequest.save
  end

  def update
    #CN edits. Update now handles offers (via act param) as well as updates to item info	
	if(params[:act] !=nil) #If false, update relates to item info so skip to line 89
		fid = Integer(params[:act])
		logger.debug "READING OFFER PARAMS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		@user = User.all()
		@itemrequest = Itemrequest.find_by_id(params[:id])
		if(fid == 1)
			logger.debug "ADD OFFER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
			@itemrequest.offeredby = current_user.id 
			#@itemrequest.update_attributes(:offeredby => 3) #this does not do anything!!!
			#@itemrequest.update_attributes(:weight => 3) #this works though
		else #(fid == 0)
			logger.debug "REMOVE OFFER!!!!!!!!!!!!!!!!!!!!!!!!!"
			@itemrequest.offeredby = nil
		end
		
		if @itemrequest.save
			redirect_to itemrequests_path
			flash[:notice] = "Offer submitted."
		else
			flash[:notice] = "Unable to submit offer."
			redirect_to root_url
		end
	else #update itemrequest info as before
	   logger.debug "AS BEFORE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
       @itemrequest = Itemrequest.find(params[:id])
       if @itemrequest.update_attributes(params[:itemrequest])
        redirect_to @itemrequest, :notice  => "Successfully updated itemrequest."
		logger.debug "UPDATE ITEM INFOS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
      else
	    logger.debug "RENDERING EDIT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        render :action => 'edit'
      end
	end
	  
	  
	  
  end
  
  def offer
	#CN edits
	if(params[:act] !=nil)
		fid = Integer(params[:act])
	end
	logger.debug "update offer by traveller"
	@user = User.all()
	@itemrequest = Itemrequest.find_by_id(params[:id])
	if(fid == 1)
		logger.debug "add offer"
		@itemrequest.offeredby = current_user.id 
		#@itemrequest.update_attributes(:offeredby => 3) #this does not do anything!!!
		#@itemrequest.update_attributes(:weight => 3) #this works though
	elsif(fid == 0)
		logger.debug "remove offer"
		@itemrequest.offeredby = Integer(nil)
	else
		logger.debug "no item"
	end	

	if @itemrequest.save
		#flash[:notice] = "Added friend."
		#redirect_to root_url
		redirect_to itemrequests_path
		flash[:notice] = "Offer submitted."
	else
		flash[:notice] = "Unable to submit offer."
		redirect_to root_url
	end

	#try this
	#@itemrequest = Itemrequest.find(params[:id])
    #if @itemrequest.update_attributes(:offeredby => 3)
    #  redirect_to itemrequests_path, :notice  => "Offer submitted."
    #else
    #  #render :action => 'edit'
	#  flash[:notice] = "Unable to submit offer."
    #end
	
	
  end

  def destroy
    @itemrequest = Itemrequest.find(params[:id])
    @itemrequest.destroy
    redirect_to itemrequests_url, :notice => "Successfully destroyed itemrequest."
  end
end
