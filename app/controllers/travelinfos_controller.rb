class TravelinfosController < ApplicationController
  def index
    @travelinfos = Travelinfo.all
  end

  def show
    @travelinfo = Travelinfo.find(params[:id])
  end

  def new
    @travelinfo = Travelinfo.new
  end

  def create
    @travelinfo = Travelinfo.new(params[:travelinfo])
    if @travelinfo.save
      redirect_to @travelinfo, :notice => "Successfully created travelinfo."
    else
      render :action => 'new'
    end
  end

  def edit
    @travelinfo = Travelinfo.find(params[:id])
  end

  def update
    @travelinfo = Travelinfo.find(params[:id])
    if @travelinfo.update_attributes(params[:travelinfo])
      redirect_to @travelinfo, :notice  => "Successfully updated travelinfo."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @travelinfo = Travelinfo.find(params[:id])
    @travelinfo.destroy
    redirect_to travelinfos_url, :notice => "Successfully destroyed travelinfo."
  end
end
