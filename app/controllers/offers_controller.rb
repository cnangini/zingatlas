class OffersController < ApplicationController
  def index
    if params[:itemrequest].nil?
      @offers = Offer.where(:user_id => current_user.id) #selects offers made to current user
	  @offersmade=1
	else
	  @offers = Offer.where(:itemrequest_id => params[:itemrequest]) #selects current user's offers to friends
	  @offersmade=0
    end	
  end

  def show
     @offer = Offer.find(params[:id])
  end

  def new
    @offer = Offer.new
	logger.debug "IN NEW!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		
	@itemrequest = Itemrequest.find_by_id(params[:id])
	@offer.itemrequest_id = @itemrequest.id 
	
	@user = User.all()
	@offer.user_id = current_user.id 

   if @offer.save
      #redirect_to @offer, :notice => "Successfully created offer."
	  logger.debug "ADDED ITEMREQUEST ID!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    else
      render :action => 'new'
    end

	
  end

  def create
    @offer = Offer.new(params[:offer])
	@itemrequest = Itemrequest.find_by_id(params[:id])
	
	@user = User.all()
	#@offer = Offer.find_by_id(params[:id])
	logger.debug "IN CREATE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
	@offer.user_id = current_user.id 
	
	
    if @offer.save
      redirect_to @offer, :notice => "Successfully created offer."
	  logger.debug "ADDED USER ID!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    else
      render :action => 'new'
    end
	
	
  end

  def edit
    @offer = Offer.find(params[:id])
  end

  def update
    @offer = Offer.find(params[:id])
    if @offer.update_attributes(params[:offer])
      redirect_to @offer, :notice  => "Successfully updated offer."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @offer = Offer.find(params[:id])
    @offer.destroy
    redirect_to itemrequests_path, :notice => "Successfully destroyed offer."
  end
end
