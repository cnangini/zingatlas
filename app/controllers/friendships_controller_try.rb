class FriendshipsController < ApplicationController
  def create
    #check if user is already a friend then return error, else send friend request ##### add as friend
	@userfriends = current_user.friendships
	@flag_req = 0
	
	@user = current_user
	
	fid = Integer(params[:friend_id])

	#make friends if current user accepts friend request from other user
	if pending_inverse == true
	  @friendship = current_user.friendships.build(:friend_id => params[:friend_id], :pending => 1)
		if @friendship.save
		  #flash[:notice] = "Added friend."
		  #redirect_to root_url
		  redirect_to userlist_path
		  flash[:notice] = "Friend request sent."
		else
		  flash[:notice] = "Unable to add friend."
		  redirect_to root_url
		end
	end
	
	#checkif friend to be added has already sent a friend request to current user
	

	
	@userfriends.each do |ufr|
		if (ufr.friend_id == fid)
			@flag_req = 1
			#logger.debug "found "
		#else
			#logger.debug "looking, but not found yet #{ufr.friend_id} and #{fid}"
		end
	end

	if @flag_req == 1
		flash[:notice] = "Already friend."
		redirect_to userlist_path
		logger.debug "User already exist"
	else
		@friendship = current_user.friendships.build(:friend_id => params[:friend_id], :pending => 1)
		if @friendship.save
		  #flash[:notice] = "Added friend."
		  #redirect_to root_url
		  redirect_to userlist_path
		  flash[:notice] = "Friend request sent."
		else
		  flash[:notice] = "Unable to add friend."
		  redirect_to root_url
		end
	end
	
  end
  
  def destroy
    @friendship = current_user.friendships.find(params[:id])
    @friendship.destroy
    flash[:notice] = "Removed friendship."
    redirect_to userlist_path #current_user
  end
  
end
