class PagesController < ApplicationController
  def landing
  end
  
  def userlist

    #Miika Pihjala:   
    # Initialize all the data you need in the view rendering here.
    # I'll do only the unconnected_users, but you should basically move
    # all the initializations like this
    #   <% pi = pending_inverse %>
    # to this controller. To refer there variables in the view, they must
    # be instance varianbles, i.e. starting with @
    #
    # unconnected is now implemented in User model
    # we also give it as parameter params[:search], that comes from
    # the search form
    @unconnected_users = current_user.unconnected(params[:search])
	@pending_inverse_users = current_user.pending_inverse
	@pending_forward_users = current_user.pending_forward
	@my_friendlist_users = current_user.my_friendlist

  end
  
  def offerdetails
  end


end
