class User < ActiveRecord::Base
  has_many :authentications, :dependent => :destroy
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable #,: confirmable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  
  has_many :friendships
  has_many :friends, :through => :friendships
  
  has_many :inverse_friendships, :class_name => "Friendship", :foreign_key => "friend_id"
  has_many :inverse_friends, :through => :inverse_friendships, :source => :user
  
  has_many :itemrequests
  has_many :inverse_itemrequests, :class_name => "Itemrequests", :foreign_key => "uid"
  #uid = id of person who is delivering the current_user's item request
  
  has_many :offers, :through => :itemrequests
	
  def self.find_for_omniauth(omniauth, signed_in_user=nil)
     if auth = Authentication.find_by_provider_and_uid(omniauth.provider, omniauth.uid) 
        # known authentication -- return the corresponding user
        auth.user
     elsif email = omniauth.info.email
        # look for a user same email as in the authenticationd data
        if user = self.find_by_email(email)
           user.apply_omniauth(omniauth)
        else 
           user = self.new(:email=>email, :password => Devise.friendly_token[0,20]);
           user.apply_omniauth(omniauth)
           user.save!
        end
        user
     else
        nil
     end
  end
         
  def apply_omniauth(omniauth)
    authentications.build(:provider => omniauth.provider, :uid => omniauth.uid, :email => omniauth.info.email, :name => omniauth.info.name, :token => omniauth.credentials.token)
  end

  #validate a password only when there is no other means of validation, ie when user is not using Facebook to login
  def password_required?
    (authentications.empty? || !password.blank?) && super
  end

  # Miika Pihjala: Moved this method from friendship_helper to here, so it can be
  # called from the controller. search will be the string that was
  # written to search field
  def unconnected(search)

    # what used to be current_user is now self, since we are calling
    # this method on current_user in the controller. That means we are
    # now operating _inside_ the current_user object.
    @user = if search
      User.where("email LIKE ?", "%#{search}%")
    else
      User.all
    end
    @cuf = self.friendships
    @requester = self.inverse_friendships.find(:all)

    #Outputs the emails of all unconnected users.
    #Logic: initialize a flag array to zero, and change entry to 1 if user has either been 
    #sent a friend request by current_user, or has sent a friend request to current_user 

    flist=[]
    #initialize flag array 
    flaguser_array=[] 
    @user.each_with_index do |f, index| 
      flaguser_array[index] = 0 
    end

    #flag users who were sent friend requests by current_user 
    @cuf.each do |c|
      ux = User.find_by_id(c.friend_id)

      @user.each_with_index do |f, index| 
        if f.email == ux.email 
        flaguser_array[index] = 1
      end 
      end
    end

    #flag users who sent friend requests to current_user 
    @requester.each do |r| 
        ind = r.user_id - 1 #NB: array indices start at 0!!! 
        flaguser_array[ind] = 1
      end 

    #save only users whose corresponding flaguser_array index is 0
    @user.each_with_index do |f, index| 
      if flaguser_array[index] == 0 
        if f.email != self.email 
      #flist << f.email	
          flist << f		  
      end
      end
    end

    flist
  end
  
  
  #CN moved from friendships_helper.rb      
  def pending_inverse
    #@requester = current_user.inverse_friendships
	@requester = self.inverse_friendships
	
	flist=[]
	@requester.each do |r| 
      if r.pending == 1 
        ux = User.find_by_id(r.user_id) 
	    flist << [ux.email, r] 
      end 
	end
    flist
  end
  
  #CN: moved from friendships_helper.rb. See pages_controller.rb for initialization.
  def pending_forward
    #this function returns email of current user's friend if
    #current user is in friendships table with selected friend (forward relationship)
		
    #@ufr = current_user.friendships 
	@ufr = self.friendships
	#finfo = Hash.new
	
	#fid = Integer(params[:friend_id])
	
	#check forward relationship
	flist = []	  
    @ufr.each do |f|  
      if f.pending == 1	
      #return f.friend_id
	    ux = User.find_by_id(f.friend_id) 	
	    flist << [ux, f] #ux.email	
	  end
    end
	flist

  end
  
  #CN: moved from friendships_helper.rb. See pages_controller.rb for initialization.
  def my_friendlist
    #@cuf = current_user.friendships
	@cuf = self.friendships
	@user = User.all()
	#@requester = current_user.inverse_friendships.find(:all)
	@requester = self.inverse_friendships.find(:all)
	
	flist=[]
	
	#check if current users's sent friend requests have been accepted
      @cuf.each do |f|  
        if f.pending == 2 
	      ux = User.find_by_id(f.friend_id) 
	      #flist << ux # ux.email
		  flist << [ux, f] # ux.email
		end  
	  end 
    
  
    #check if current users has accepted incoming friend requests 
	if @requester.count > 0 
      @requester.each do |r, index| 
	    if r.pending == 2 
	      uxi = User.find_by_id(r.user_id)
	      #flist << uxi 
		  flist << [uxi, r]
	    end      		
      end  
    end 

	flist
	
  end

end
