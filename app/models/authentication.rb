require 'open-uri'
class Authentication < ActiveRecord::Base
  belongs_to :user
  attr_accessible :provider, :uid, :email, :name, :token

  def find_friends()
    url = "https://graph.facebook.com/#{uid}/friends?access_token=#{token}"
    if provider == 'facebook' and uid and token
         result = JSON.parse(open(url).read)
    end
    return result
  end

end
