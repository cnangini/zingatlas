class Travelinfo < ActiveRecord::Base
  attr_accessible :user_id, :uid, :origin, :destination, :departdate, :returndate, :willship
end
