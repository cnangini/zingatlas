require 'test_helper'

class TravelinfosControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => Travelinfo.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    Travelinfo.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    Travelinfo.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to travelinfo_url(assigns(:travelinfo))
  end

  def test_edit
    get :edit, :id => Travelinfo.first
    assert_template 'edit'
  end

  def test_update_invalid
    Travelinfo.any_instance.stubs(:valid?).returns(false)
    put :update, :id => Travelinfo.first
    assert_template 'edit'
  end

  def test_update_valid
    Travelinfo.any_instance.stubs(:valid?).returns(true)
    put :update, :id => Travelinfo.first
    assert_redirected_to travelinfo_url(assigns(:travelinfo))
  end

  def test_destroy
    travelinfo = Travelinfo.first
    delete :destroy, :id => travelinfo
    assert_redirected_to travelinfos_url
    assert !Travelinfo.exists?(travelinfo.id)
  end
end
