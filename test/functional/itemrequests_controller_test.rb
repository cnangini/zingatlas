require 'test_helper'

class ItemrequestsControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => Itemrequest.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    Itemrequest.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    Itemrequest.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to itemrequest_url(assigns(:itemrequest))
  end

  def test_edit
    get :edit, :id => Itemrequest.first
    assert_template 'edit'
  end

  def test_update_invalid
    Itemrequest.any_instance.stubs(:valid?).returns(false)
    put :update, :id => Itemrequest.first
    assert_template 'edit'
  end

  def test_update_valid
    Itemrequest.any_instance.stubs(:valid?).returns(true)
    put :update, :id => Itemrequest.first
    assert_redirected_to itemrequest_url(assigns(:itemrequest))
  end

  def test_destroy
    itemrequest = Itemrequest.first
    delete :destroy, :id => itemrequest
    assert_redirected_to itemrequests_url
    assert !Itemrequest.exists?(itemrequest.id)
  end
end
