PUBLIC_DIR = if defined?($servlet_context)

     $servlet_context.getRealPath('/').gsub(/^\w:/, "").gsub(/\\/, "/") 

   else

     "#{Rails.root}" + '/public'

   end